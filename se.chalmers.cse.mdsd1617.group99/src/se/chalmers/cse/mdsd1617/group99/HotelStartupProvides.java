/**
 */
package se.chalmers.cse.mdsd1617.group99;

import se.chalmers.cse.mdsd1617.bookingInterface.IHotelStartupProvides;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hotel Startup Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group99.Group99Package#getHotelStartupProvides()
 * @model
 * @generated
 */
public interface HotelStartupProvides extends IHotelStartupProvides {
} // HotelStartupProvides
