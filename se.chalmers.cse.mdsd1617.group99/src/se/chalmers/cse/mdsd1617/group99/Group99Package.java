/**
 */
package se.chalmers.cse.mdsd1617.group99;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import se.chalmers.cse.mdsd1617.bookingInterface.BookingInterfacePackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see se.chalmers.cse.mdsd1617.group99.Group99Factory
 * @model kind="package"
 * @generated
 */
public interface Group99Package extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "group99";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///se/chalmers/cse/mdsd1617/group99.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "se.chalmers.cse.mdsd1617.group99";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Group99Package eINSTANCE = se.chalmers.cse.mdsd1617.group99.impl.Group99PackageImpl.init();

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group99.impl.HotelCustomerProvidesImpl <em>Hotel Customer Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group99.impl.HotelCustomerProvidesImpl
	 * @see se.chalmers.cse.mdsd1617.group99.impl.Group99PackageImpl#getHotelCustomerProvides()
	 * @generated
	 */
	int HOTEL_CUSTOMER_PROVIDES = 0;

	/**
	 * The number of structural features of the '<em>Hotel Customer Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT = BookingInterfacePackage.IHOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING = BookingInterfacePackage.IHOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Book Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___BOOK_ROOM__INT_STRING_STRING = BookingInterfacePackage.IHOTEL_CUSTOMER_PROVIDES___BOOK_ROOM__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Confirm Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___CONFIRM_ROOM__INT_STRING_STRING = BookingInterfacePackage.IHOTEL_CUSTOMER_PROVIDES___CONFIRM_ROOM__INT_STRING_STRING;

	/**
	 * The number of operations of the '<em>Hotel Customer Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT = BookingInterfacePackage.IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group99.impl.HotelStartupProvidesImpl <em>Hotel Startup Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group99.impl.HotelStartupProvidesImpl
	 * @see se.chalmers.cse.mdsd1617.group99.impl.Group99PackageImpl#getHotelStartupProvides()
	 * @generated
	 */
	int HOTEL_STARTUP_PROVIDES = 1;

	/**
	 * The number of structural features of the '<em>Hotel Startup Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_STARTUP_PROVIDES_FEATURE_COUNT = BookingInterfacePackage.IHOTEL_STARTUP_PROVIDES_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Startup</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_STARTUP_PROVIDES___STARTUP__INT = BookingInterfacePackage.IHOTEL_STARTUP_PROVIDES___STARTUP__INT;

	/**
	 * The number of operations of the '<em>Hotel Startup Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_STARTUP_PROVIDES_OPERATION_COUNT = BookingInterfacePackage.IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group99.HotelCustomerProvides <em>Hotel Customer Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hotel Customer Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group99.HotelCustomerProvides
	 * @generated
	 */
	EClass getHotelCustomerProvides();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group99.HotelStartupProvides <em>Hotel Startup Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hotel Startup Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group99.HotelStartupProvides
	 * @generated
	 */
	EClass getHotelStartupProvides();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Group99Factory getGroup99Factory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group99.impl.HotelCustomerProvidesImpl <em>Hotel Customer Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group99.impl.HotelCustomerProvidesImpl
		 * @see se.chalmers.cse.mdsd1617.group99.impl.Group99PackageImpl#getHotelCustomerProvides()
		 * @generated
		 */
		EClass HOTEL_CUSTOMER_PROVIDES = eINSTANCE.getHotelCustomerProvides();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group99.impl.HotelStartupProvidesImpl <em>Hotel Startup Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group99.impl.HotelStartupProvidesImpl
		 * @see se.chalmers.cse.mdsd1617.group99.impl.Group99PackageImpl#getHotelStartupProvides()
		 * @generated
		 */
		EClass HOTEL_STARTUP_PROVIDES = eINSTANCE.getHotelStartupProvides();

	}

} //Group99Package
