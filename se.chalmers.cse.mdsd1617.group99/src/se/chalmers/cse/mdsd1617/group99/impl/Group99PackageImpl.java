/**
 */
package se.chalmers.cse.mdsd1617.group99.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.uml2.types.TypesPackage;

import org.eclipse.uml2.types.impl.TypesPackageImpl;

import se.chalmers.cse.mdsd1617.bookingInterface.BookingInterfacePackage;

import se.chalmers.cse.mdsd1617.bookingInterface.impl.BookingInterfacePackageImpl;

import se.chalmers.cse.mdsd1617.group99.Group99Factory;
import se.chalmers.cse.mdsd1617.group99.Group99Package;
import se.chalmers.cse.mdsd1617.group99.HotelCustomerProvides;
import se.chalmers.cse.mdsd1617.group99.HotelStartupProvides;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Group99PackageImpl extends EPackageImpl implements Group99Package {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hotelCustomerProvidesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hotelStartupProvidesEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see se.chalmers.cse.mdsd1617.group99.Group99Package#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private Group99PackageImpl() {
		super(eNS_URI, Group99Factory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link Group99Package#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static Group99Package init() {
		if (isInited) return (Group99Package)EPackage.Registry.INSTANCE.getEPackage(Group99Package.eNS_URI);

		// Obtain or create and register package
		Group99PackageImpl theGroup99Package = (Group99PackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof Group99PackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new Group99PackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		BookingInterfacePackageImpl theBookingInterfacePackage = (BookingInterfacePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BookingInterfacePackage.eNS_URI) instanceof BookingInterfacePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BookingInterfacePackage.eNS_URI) : BookingInterfacePackage.eINSTANCE);
		TypesPackageImpl theTypesPackage = (TypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI) instanceof TypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI) : TypesPackage.eINSTANCE);

		// Create package meta-data objects
		theGroup99Package.createPackageContents();
		theBookingInterfacePackage.createPackageContents();
		theTypesPackage.createPackageContents();

		// Initialize created meta-data
		theGroup99Package.initializePackageContents();
		theBookingInterfacePackage.initializePackageContents();
		theTypesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theGroup99Package.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(Group99Package.eNS_URI, theGroup99Package);
		return theGroup99Package;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHotelCustomerProvides() {
		return hotelCustomerProvidesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHotelStartupProvides() {
		return hotelStartupProvidesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Group99Factory getGroup99Factory() {
		return (Group99Factory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		hotelCustomerProvidesEClass = createEClass(HOTEL_CUSTOMER_PROVIDES);

		hotelStartupProvidesEClass = createEClass(HOTEL_STARTUP_PROVIDES);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		BookingInterfacePackage theBookingInterfacePackage = (BookingInterfacePackage)EPackage.Registry.INSTANCE.getEPackage(BookingInterfacePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		hotelCustomerProvidesEClass.getESuperTypes().add(theBookingInterfacePackage.getIHotelCustomerProvides());
		hotelStartupProvidesEClass.getESuperTypes().add(theBookingInterfacePackage.getIHotelStartupProvides());

		// Initialize classes, features, and operations; add parameters
		initEClass(hotelCustomerProvidesEClass, HotelCustomerProvides.class, "HotelCustomerProvides", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(hotelStartupProvidesEClass, HotelStartupProvides.class, "HotelStartupProvides", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //Group99PackageImpl
