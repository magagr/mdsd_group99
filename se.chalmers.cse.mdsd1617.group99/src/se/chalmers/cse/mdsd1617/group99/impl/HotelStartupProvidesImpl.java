/**
 */
package se.chalmers.cse.mdsd1617.group99.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group99.Group99Package;
import se.chalmers.cse.mdsd1617.group99.HotelStartupProvides;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hotel Startup Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class HotelStartupProvidesImpl extends MinimalEObjectImpl.Container implements HotelStartupProvides {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HotelStartupProvidesImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Group99Package.Literals.HOTEL_STARTUP_PROVIDES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void startup(int numRooms) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case Group99Package.HOTEL_STARTUP_PROVIDES___STARTUP__INT:
				startup((Integer)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //HotelStartupProvidesImpl
