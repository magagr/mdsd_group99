/**
 */
package se.chalmers.cse.mdsd1617.group99;

import se.chalmers.cse.mdsd1617.bookingInterface.IHotelCustomerProvides;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hotel Customer Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group99.Group99Package#getHotelCustomerProvides()
 * @model
 * @generated
 */
public interface HotelCustomerProvides extends IHotelCustomerProvides {
} // HotelCustomerProvides
