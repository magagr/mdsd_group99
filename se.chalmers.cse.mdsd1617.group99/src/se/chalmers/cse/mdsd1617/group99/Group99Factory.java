/**
 */
package se.chalmers.cse.mdsd1617.group99;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see se.chalmers.cse.mdsd1617.group99.Group99Package
 * @generated
 */
public interface Group99Factory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Group99Factory eINSTANCE = se.chalmers.cse.mdsd1617.group99.impl.Group99FactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Hotel Customer Provides</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Hotel Customer Provides</em>'.
	 * @generated
	 */
	HotelCustomerProvides createHotelCustomerProvides();

	/**
	 * Returns a new object of class '<em>Hotel Startup Provides</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Hotel Startup Provides</em>'.
	 * @generated
	 */
	HotelStartupProvides createHotelStartupProvides();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	Group99Package getGroup99Package();

} //Group99Factory
