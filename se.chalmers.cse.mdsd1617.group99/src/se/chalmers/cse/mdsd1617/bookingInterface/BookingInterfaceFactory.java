/**
 */
package se.chalmers.cse.mdsd1617.bookingInterface;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see se.chalmers.cse.mdsd1617.bookingInterface.BookingInterfacePackage
 * @generated
 */
public interface BookingInterfaceFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	BookingInterfaceFactory eINSTANCE = se.chalmers.cse.mdsd1617.bookingInterface.impl.BookingInterfaceFactoryImpl.init();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	BookingInterfacePackage getBookingInterfacePackage();

} //BookingInterfaceFactory
