/**
 */
package se.chalmers.cse.mdsd1617.bookingInterface;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IHotel Startup Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.bookingInterface.BookingInterfacePackage#getIHotelStartupProvides()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IHotelStartupProvides extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model numRoomsDataType="org.eclipse.uml2.types.Integer" numRoomsRequired="true" numRoomsOrdered="false"
	 * @generated
	 */
	void startup(int numRooms);

} // IHotelStartupProvides
