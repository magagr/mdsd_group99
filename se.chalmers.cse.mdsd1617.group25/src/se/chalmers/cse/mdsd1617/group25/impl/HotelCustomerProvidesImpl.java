/**
 */
package se.chalmers.cse.mdsd1617.group25.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

import javax.xml.soap.SOAPException;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires;
import se.chalmers.cse.mdsd1617.group25.FreeRoomTypesDTO;
import se.chalmers.cse.mdsd1617.group25.Group25Factory;
import se.chalmers.cse.mdsd1617.group25.Group25Package;
import se.chalmers.cse.mdsd1617.group25.HotelCustomerProvides;
import se.chalmers.cse.mdsd1617.group25.residence.IResidenceProvider;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hotel Customer Provides</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group25.impl.HotelCustomerProvidesImpl#getResidenceProvider <em>Residence Provider</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HotelCustomerProvidesImpl extends MinimalEObjectImpl.Container implements HotelCustomerProvides {
	/**
	 * The cached value of the '{@link #getResidenceProvider() <em>Residence Provider</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResidenceProvider()
	 * @generated
	 * @ordered
	 */
	protected IResidenceProvider residenceProvider;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HotelCustomerProvidesImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Group25Package.Literals.HOTEL_CUSTOMER_PROVIDES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IResidenceProvider getResidenceProvider() {
		return residenceProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetResidenceProvider(IResidenceProvider newResidenceProvider, NotificationChain msgs) {
		IResidenceProvider oldResidenceProvider = residenceProvider;
		residenceProvider = newResidenceProvider;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Group25Package.HOTEL_CUSTOMER_PROVIDES__RESIDENCE_PROVIDER, oldResidenceProvider, newResidenceProvider);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResidenceProvider(IResidenceProvider newResidenceProvider) {
		if (newResidenceProvider != residenceProvider) {
			NotificationChain msgs = null;
			if (residenceProvider != null)
				msgs = ((InternalEObject)residenceProvider).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - Group25Package.HOTEL_CUSTOMER_PROVIDES__RESIDENCE_PROVIDER, null, msgs);
			if (newResidenceProvider != null)
				msgs = ((InternalEObject)newResidenceProvider).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - Group25Package.HOTEL_CUSTOMER_PROVIDES__RESIDENCE_PROVIDER, null, msgs);
			msgs = basicSetResidenceProvider(newResidenceProvider, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group25Package.HOTEL_CUSTOMER_PROVIDES__RESIDENCE_PROVIDER, newResidenceProvider, newResidenceProvider));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<FreeRoomTypesDTO> getFreeRooms(int numBeds, String startDate, String endDate) {
//		EList<FreeRoomTypesDTO> free;
		FreeRoomTypesDTO rt = Group25Factory.eINSTANCE.createFreeRoomTypesDTO();
		rt.setNumBeds(numBeds);
		rt.setNumFreeRooms(residenceProvider.getRoomCount());
		
		return new BasicEList<FreeRoomTypesDTO>(Arrays.asList(rt));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int initiateBooking(String firstName, String startDate, String endDate, String lastName) {
		// TODO: implement this method
		return -1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoomToBooking(String roomTypeDescription, int bookingID) {
		// TODO: implement this method
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean confirmBooking(int bookingID) {
		// TODO: implement this method
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double initiateCheckout(int bookingID) {
		// TODO: implement this method
		return -1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payDuringCheckout(String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {
		boolean ret = false;

		//TODO: find out a sane cost
		double cost = 100;

		try {
			// Acquire CustomerRequires object
			CustomerRequires banking = CustomerRequires.instance();

			// Check for credit card validity
			ret = banking.isCreditCardValid(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName);
			if (!ret) {
				System.out.println("Invalid credit card");

			} else {
				System.out.println("Valid credit card");

				// Make a payment
				ret = banking.makePayment(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName, cost);
				if (!ret) {
					System.out.println("Payment failed - invalid credit card or insufficient credit");
				} else {
					System.out.println("Payment success!");
				}
			}


		} catch (SOAPException e) {
			System.err.println("Error occurred while communicating with the bank");
			e.printStackTrace();
		}

		return ret;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double initiateRoomCheckout(int roomNumber, int bookingId) {
		// TODO: implement this method
		return -1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payRoomDuringCheckout(int roomNumber, String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {
		// TODO: implement this method
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int checkInRoom(String roomTypeDescription, int bookingID) {
		// TODO: implement this method
		return -1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Group25Package.HOTEL_CUSTOMER_PROVIDES__RESIDENCE_PROVIDER:
				return basicSetResidenceProvider(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Group25Package.HOTEL_CUSTOMER_PROVIDES__RESIDENCE_PROVIDER:
				return getResidenceProvider();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Group25Package.HOTEL_CUSTOMER_PROVIDES__RESIDENCE_PROVIDER:
				setResidenceProvider((IResidenceProvider)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Group25Package.HOTEL_CUSTOMER_PROVIDES__RESIDENCE_PROVIDER:
				setResidenceProvider((IResidenceProvider)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Group25Package.HOTEL_CUSTOMER_PROVIDES__RESIDENCE_PROVIDER:
				return residenceProvider != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case Group25Package.HOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING:
				return getFreeRooms((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case Group25Package.HOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING:
				return initiateBooking((String)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (String)arguments.get(3));
			case Group25Package.HOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT:
				return addRoomToBooking((String)arguments.get(0), (Integer)arguments.get(1));
			case Group25Package.HOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT:
				return confirmBooking((Integer)arguments.get(0));
			case Group25Package.HOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT:
				return initiateCheckout((Integer)arguments.get(0));
			case Group25Package.HOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING:
				return payDuringCheckout((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2), (Integer)arguments.get(3), (String)arguments.get(4), (String)arguments.get(5));
			case Group25Package.HOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT:
				return initiateRoomCheckout((Integer)arguments.get(0), (Integer)arguments.get(1));
			case Group25Package.HOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING:
				return payRoomDuringCheckout((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (Integer)arguments.get(3), (Integer)arguments.get(4), (String)arguments.get(5), (String)arguments.get(6));
			case Group25Package.HOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT:
				return checkInRoom((String)arguments.get(0), (Integer)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

} //HotelCustomerProvidesImpl
