/**
 */
package se.chalmers.cse.mdsd1617.group25.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import se.chalmers.cse.mdsd1617.group25.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Group25FactoryImpl extends EFactoryImpl implements Group25Factory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Group25Factory init() {
		try {
			Group25Factory theGroup25Factory = (Group25Factory)EPackage.Registry.INSTANCE.getEFactory(Group25Package.eNS_URI);
			if (theGroup25Factory != null) {
				return theGroup25Factory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new Group25FactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Group25FactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case Group25Package.FREE_ROOM_TYPES_DTO: return createFreeRoomTypesDTO();
			case Group25Package.HOTEL_CUSTOMER_PROVIDES: return createHotelCustomerProvides();
			case Group25Package.HOTEL_STARTUP_PROVIDES: return createHotelStartupProvides();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FreeRoomTypesDTO createFreeRoomTypesDTO() {
		FreeRoomTypesDTOImpl freeRoomTypesDTO = new FreeRoomTypesDTOImpl();
		return freeRoomTypesDTO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public HotelCustomerProvides createHotelCustomerProvides() {
		return HotelStartupProvidesImpl.getSystem();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HotelStartupProvides createHotelStartupProvides() {
		HotelStartupProvidesImpl hotelStartupProvides = new HotelStartupProvidesImpl();
		return hotelStartupProvides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Group25Package getGroup25Package() {
		return (Group25Package)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static Group25Package getPackage() {
		return Group25Package.eINSTANCE;
	}

} //Group25FactoryImpl
