/**
 */
package se.chalmers.cse.mdsd1617.group25.impl;

import java.lang.reflect.InvocationTargetException;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import se.chalmers.cse.mdsd1617.group25.Group25Package;
import se.chalmers.cse.mdsd1617.group25.HotelCustomerProvides;
import se.chalmers.cse.mdsd1617.group25.HotelStartupProvides;
import se.chalmers.cse.mdsd1617.group25.residence.ResidenceFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hotel Startup Provides</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group25.impl.HotelStartupProvidesImpl#getTheSystem <em>The System</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HotelStartupProvidesImpl extends MinimalEObjectImpl.Container implements HotelStartupProvides {
	/**
	 * The cached value of the '{@link #getTheSystem() <em>The System</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTheSystem()
	 * @generated NOT
	 * @ordered
	 */
	private static HotelCustomerProvides theSystem;

	public static HotelCustomerProvides getSystem() { return theSystem; }

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HotelStartupProvidesImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Group25Package.Literals.HOTEL_STARTUP_PROVIDES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HotelCustomerProvides getTheSystem() {
		if (theSystem != null && theSystem.eIsProxy()) {
			InternalEObject oldTheSystem = (InternalEObject)theSystem;
			theSystem = (HotelCustomerProvides)eResolveProxy(oldTheSystem);
			if (theSystem != oldTheSystem) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group25Package.HOTEL_STARTUP_PROVIDES__THE_SYSTEM, oldTheSystem, theSystem));
			}
		}
		return theSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HotelCustomerProvides basicGetTheSystem() {
		return theSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTheSystem(HotelCustomerProvides newTheSystem) {
		HotelCustomerProvides oldTheSystem = theSystem;
		theSystem = newTheSystem;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group25Package.HOTEL_STARTUP_PROVIDES__THE_SYSTEM, oldTheSystem, theSystem));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void startup(int numRooms) {
		setTheSystem(new HotelCustomerProvidesImpl());
		theSystem.setResidenceProvider(ResidenceFactory.eINSTANCE.createResidenceProvider());
		
		for (int i=0; i<numRooms; i++) {
			theSystem.getResidenceProvider().addRoom(0, i);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Group25Package.HOTEL_STARTUP_PROVIDES__THE_SYSTEM:
				if (resolve) return getTheSystem();
				return basicGetTheSystem();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Group25Package.HOTEL_STARTUP_PROVIDES__THE_SYSTEM:
				setTheSystem((HotelCustomerProvides)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Group25Package.HOTEL_STARTUP_PROVIDES__THE_SYSTEM:
				setTheSystem((HotelCustomerProvides)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Group25Package.HOTEL_STARTUP_PROVIDES__THE_SYSTEM:
				return theSystem != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case Group25Package.HOTEL_STARTUP_PROVIDES___STARTUP__INT:
				startup((Integer)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //HotelStartupProvidesImpl
