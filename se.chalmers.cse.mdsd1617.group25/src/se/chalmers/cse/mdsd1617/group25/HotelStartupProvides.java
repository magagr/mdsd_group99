/**
 */
package se.chalmers.cse.mdsd1617.group25;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hotel Startup Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group25.HotelStartupProvides#getTheSystem <em>The System</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group25.Group25Package#getHotelStartupProvides()
 * @model
 * @generated
 */
public interface HotelStartupProvides extends IHotelStartupProvides {

	/**
	 * Returns the value of the '<em><b>The System</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>The System</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>The System</em>' reference.
	 * @see #setTheSystem(HotelCustomerProvides)
	 * @see se.chalmers.cse.mdsd1617.group25.Group25Package#getHotelStartupProvides_TheSystem()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	HotelCustomerProvides getTheSystem();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group25.HotelStartupProvides#getTheSystem <em>The System</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>The System</em>' reference.
	 * @see #getTheSystem()
	 * @generated
	 */
	void setTheSystem(HotelCustomerProvides value);
} // HotelStartupProvides
