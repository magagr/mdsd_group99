/**
 */
package se.chalmers.cse.mdsd1617.group25.residence;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Room Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group25.residence.ResidencePackage#getRoomType()
 * @model
 * @generated
 */
public interface RoomType extends EObject {

} // RoomType
