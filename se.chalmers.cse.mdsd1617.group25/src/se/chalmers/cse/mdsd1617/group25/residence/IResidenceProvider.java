/**
 */
package se.chalmers.cse.mdsd1617.group25.residence;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IResidence Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group25.residence.ResidencePackage#getIResidenceProvider()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IResidenceProvider extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" typeRequired="true" typeOrdered="false" numRequired="true" numOrdered="false"
	 * @generated
	 */
	boolean addRoom(int type, int num);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true" ordered="false"
	 * @generated
	 */
	int getRoomCount();

} // IResidenceProvider
