/**
 */
package se.chalmers.cse.mdsd1617.group25.residence.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import se.chalmers.cse.mdsd1617.group25.residence.ResidenceFactory;
import se.chalmers.cse.mdsd1617.group25.residence.ResidencePackage;
import se.chalmers.cse.mdsd1617.group25.residence.ResidenceProvider;
import se.chalmers.cse.mdsd1617.group25.residence.Room;
import se.chalmers.cse.mdsd1617.group25.residence.RoomType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Provider</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group25.residence.impl.ResidenceProviderImpl#getRooms <em>Rooms</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group25.residence.impl.ResidenceProviderImpl#getRoomTypes <em>Room Types</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ResidenceProviderImpl extends MinimalEObjectImpl.Container implements ResidenceProvider {
	/**
	 * The cached value of the '{@link #getRooms() <em>Rooms</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRooms()
	 * @generated
	 * @ordered
	 */
	protected EList<Room> rooms;

	/**
	 * The cached value of the '{@link #getRoomTypes() <em>Room Types</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<RoomType> roomTypes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ResidenceProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ResidencePackage.Literals.RESIDENCE_PROVIDER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Room> getRooms() {
		if (rooms == null) {
			rooms = new EObjectResolvingEList<Room>(Room.class, this, ResidencePackage.RESIDENCE_PROVIDER__ROOMS);
		}
		return rooms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RoomType> getRoomTypes() {
		if (roomTypes == null) {
			roomTypes = new EObjectResolvingEList<RoomType>(RoomType.class, this, ResidencePackage.RESIDENCE_PROVIDER__ROOM_TYPES);
		}
		return roomTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoom(int type, int num) {
		if (getRooms().isEmpty()) {
			Room room = ResidenceFactory.eINSTANCE.createRoom();
			room.setNum(num);
			room.setRoomtype(ResidenceFactory.eINSTANCE.createRoomType()); //TODO: addRoomType
			rooms.add(room);
		}
		
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int getRoomCount() {
		return rooms.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ResidencePackage.RESIDENCE_PROVIDER__ROOMS:
				return getRooms();
			case ResidencePackage.RESIDENCE_PROVIDER__ROOM_TYPES:
				return getRoomTypes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ResidencePackage.RESIDENCE_PROVIDER__ROOMS:
				getRooms().clear();
				getRooms().addAll((Collection<? extends Room>)newValue);
				return;
			case ResidencePackage.RESIDENCE_PROVIDER__ROOM_TYPES:
				getRoomTypes().clear();
				getRoomTypes().addAll((Collection<? extends RoomType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ResidencePackage.RESIDENCE_PROVIDER__ROOMS:
				getRooms().clear();
				return;
			case ResidencePackage.RESIDENCE_PROVIDER__ROOM_TYPES:
				getRoomTypes().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ResidencePackage.RESIDENCE_PROVIDER__ROOMS:
				return rooms != null && !rooms.isEmpty();
			case ResidencePackage.RESIDENCE_PROVIDER__ROOM_TYPES:
				return roomTypes != null && !roomTypes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ResidencePackage.RESIDENCE_PROVIDER___ADD_ROOM__INT_INT:
				return addRoom((Integer)arguments.get(0), (Integer)arguments.get(1));
			case ResidencePackage.RESIDENCE_PROVIDER___GET_ROOM_COUNT:
				return getRoomCount();
		}
		return super.eInvoke(operationID, arguments);
	}

} //ResidenceProviderImpl
