/**
 */
package se.chalmers.cse.mdsd1617.group25.residence.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;
import se.chalmers.cse.mdsd1617.group25.Group25Package;

import se.chalmers.cse.mdsd1617.group25.impl.Group25PackageImpl;

import se.chalmers.cse.mdsd1617.group25.residence.IResidenceProvider;
import se.chalmers.cse.mdsd1617.group25.residence.ResidenceFactory;
import se.chalmers.cse.mdsd1617.group25.residence.ResidencePackage;
import se.chalmers.cse.mdsd1617.group25.residence.ResidenceProvider;
import se.chalmers.cse.mdsd1617.group25.residence.Room;
import se.chalmers.cse.mdsd1617.group25.residence.RoomType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ResidencePackageImpl extends EPackageImpl implements ResidencePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass residenceProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iResidenceProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see se.chalmers.cse.mdsd1617.group25.residence.ResidencePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ResidencePackageImpl() {
		super(eNS_URI, ResidenceFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ResidencePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ResidencePackage init() {
		if (isInited) return (ResidencePackage)EPackage.Registry.INSTANCE.getEPackage(ResidencePackage.eNS_URI);

		// Obtain or create and register package
		ResidencePackageImpl theResidencePackage = (ResidencePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ResidencePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ResidencePackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		Group25PackageImpl theGroup25Package = (Group25PackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Group25Package.eNS_URI) instanceof Group25PackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Group25Package.eNS_URI) : Group25Package.eINSTANCE);

		// Create package meta-data objects
		theResidencePackage.createPackageContents();
		theGroup25Package.createPackageContents();

		// Initialize created meta-data
		theResidencePackage.initializePackageContents();
		theGroup25Package.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theResidencePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ResidencePackage.eNS_URI, theResidencePackage);
		return theResidencePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getResidenceProvider() {
		return residenceProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResidenceProvider_Rooms() {
		return (EReference)residenceProviderEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResidenceProvider_RoomTypes() {
		return (EReference)residenceProviderEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoomType() {
		return roomTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIResidenceProvider() {
		return iResidenceProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIResidenceProvider__AddRoom__int_int() {
		return iResidenceProviderEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIResidenceProvider__GetRoomCount() {
		return iResidenceProviderEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoom() {
		return roomEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoom_Roomtype() {
		return (EReference)roomEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoom_Num() {
		return (EAttribute)roomEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResidenceFactory getResidenceFactory() {
		return (ResidenceFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		residenceProviderEClass = createEClass(RESIDENCE_PROVIDER);
		createEReference(residenceProviderEClass, RESIDENCE_PROVIDER__ROOMS);
		createEReference(residenceProviderEClass, RESIDENCE_PROVIDER__ROOM_TYPES);

		roomEClass = createEClass(ROOM);
		createEReference(roomEClass, ROOM__ROOMTYPE);
		createEAttribute(roomEClass, ROOM__NUM);

		roomTypeEClass = createEClass(ROOM_TYPE);

		iResidenceProviderEClass = createEClass(IRESIDENCE_PROVIDER);
		createEOperation(iResidenceProviderEClass, IRESIDENCE_PROVIDER___ADD_ROOM__INT_INT);
		createEOperation(iResidenceProviderEClass, IRESIDENCE_PROVIDER___GET_ROOM_COUNT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		residenceProviderEClass.getESuperTypes().add(this.getIResidenceProvider());

		// Initialize classes, features, and operations; add parameters
		initEClass(residenceProviderEClass, ResidenceProvider.class, "ResidenceProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getResidenceProvider_Rooms(), this.getRoom(), null, "rooms", null, 0, -1, ResidenceProvider.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getResidenceProvider_RoomTypes(), this.getRoomType(), null, "roomTypes", null, 0, -1, ResidenceProvider.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(roomEClass, Room.class, "Room", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRoom_Roomtype(), this.getRoomType(), null, "roomtype", null, 1, 1, Room.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRoom_Num(), ecorePackage.getEInt(), "num", null, 1, 1, Room.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(roomTypeEClass, RoomType.class, "RoomType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(iResidenceProviderEClass, IResidenceProvider.class, "IResidenceProvider", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = initEOperation(getIResidenceProvider__AddRoom__int_int(), ecorePackage.getEBoolean(), "addRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "type", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "num", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIResidenceProvider__GetRoomCount(), ecorePackage.getEInt(), "getRoomCount", 1, 1, IS_UNIQUE, !IS_ORDERED);
	}

} //ResidencePackageImpl
