/**
 */
package se.chalmers.cse.mdsd1617.group25.residence;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see se.chalmers.cse.mdsd1617.group25.residence.ResidenceFactory
 * @model kind="package"
 * @generated
 */
public interface ResidencePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "residence";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///se/chalmers/cse/mdsd1617/group25/residence.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "se.chalmers.cse.mdsd1617.group25.residence";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ResidencePackage eINSTANCE = se.chalmers.cse.mdsd1617.group25.residence.impl.ResidencePackageImpl.init();

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group25.residence.IResidenceProvider <em>IResidence Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group25.residence.IResidenceProvider
	 * @see se.chalmers.cse.mdsd1617.group25.residence.impl.ResidencePackageImpl#getIResidenceProvider()
	 * @generated
	 */
	int IRESIDENCE_PROVIDER = 3;

	/**
	 * The number of structural features of the '<em>IResidence Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRESIDENCE_PROVIDER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRESIDENCE_PROVIDER___ADD_ROOM__INT_INT = 0;

	/**
	 * The operation id for the '<em>Get Room Count</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRESIDENCE_PROVIDER___GET_ROOM_COUNT = 1;

	/**
	 * The number of operations of the '<em>IResidence Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRESIDENCE_PROVIDER_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group25.residence.impl.ResidenceProviderImpl <em>Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group25.residence.impl.ResidenceProviderImpl
	 * @see se.chalmers.cse.mdsd1617.group25.residence.impl.ResidencePackageImpl#getResidenceProvider()
	 * @generated
	 */
	int RESIDENCE_PROVIDER = 0;

	/**
	 * The feature id for the '<em><b>Rooms</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESIDENCE_PROVIDER__ROOMS = IRESIDENCE_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Room Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESIDENCE_PROVIDER__ROOM_TYPES = IRESIDENCE_PROVIDER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESIDENCE_PROVIDER_FEATURE_COUNT = IRESIDENCE_PROVIDER_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESIDENCE_PROVIDER___ADD_ROOM__INT_INT = IRESIDENCE_PROVIDER___ADD_ROOM__INT_INT;

	/**
	 * The operation id for the '<em>Get Room Count</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESIDENCE_PROVIDER___GET_ROOM_COUNT = IRESIDENCE_PROVIDER___GET_ROOM_COUNT;

	/**
	 * The number of operations of the '<em>Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESIDENCE_PROVIDER_OPERATION_COUNT = IRESIDENCE_PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group25.residence.impl.RoomTypeImpl <em>Room Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group25.residence.impl.RoomTypeImpl
	 * @see se.chalmers.cse.mdsd1617.group25.residence.impl.ResidencePackageImpl#getRoomType()
	 * @generated
	 */
	int ROOM_TYPE = 2;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group25.residence.impl.RoomImpl <em>Room</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group25.residence.impl.RoomImpl
	 * @see se.chalmers.cse.mdsd1617.group25.residence.impl.ResidencePackageImpl#getRoom()
	 * @generated
	 */
	int ROOM = 1;

	/**
	 * The feature id for the '<em><b>Roomtype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__ROOMTYPE = 0;

	/**
	 * The feature id for the '<em><b>Num</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__NUM = 1;

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group25.residence.ResidenceProvider <em>Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Provider</em>'.
	 * @see se.chalmers.cse.mdsd1617.group25.residence.ResidenceProvider
	 * @generated
	 */
	EClass getResidenceProvider();

	/**
	 * Returns the meta object for the reference list '{@link se.chalmers.cse.mdsd1617.group25.residence.ResidenceProvider#getRooms <em>Rooms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Rooms</em>'.
	 * @see se.chalmers.cse.mdsd1617.group25.residence.ResidenceProvider#getRooms()
	 * @see #getResidenceProvider()
	 * @generated
	 */
	EReference getResidenceProvider_Rooms();

	/**
	 * Returns the meta object for the reference list '{@link se.chalmers.cse.mdsd1617.group25.residence.ResidenceProvider#getRoomTypes <em>Room Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Room Types</em>'.
	 * @see se.chalmers.cse.mdsd1617.group25.residence.ResidenceProvider#getRoomTypes()
	 * @see #getResidenceProvider()
	 * @generated
	 */
	EReference getResidenceProvider_RoomTypes();

	/**
	 * The number of structural features of the '<em>Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_OPERATION_COUNT = 0;

	/**
	 * The number of structural features of the '<em>Room Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Room Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group25.residence.RoomType <em>Room Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room Type</em>'.
	 * @see se.chalmers.cse.mdsd1617.group25.residence.RoomType
	 * @generated
	 */
	EClass getRoomType();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group25.residence.IResidenceProvider <em>IResidence Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IResidence Provider</em>'.
	 * @see se.chalmers.cse.mdsd1617.group25.residence.IResidenceProvider
	 * @generated
	 */
	EClass getIResidenceProvider();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group25.residence.IResidenceProvider#addRoom(int, int) <em>Add Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group25.residence.IResidenceProvider#addRoom(int, int)
	 * @generated
	 */
	EOperation getIResidenceProvider__AddRoom__int_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group25.residence.IResidenceProvider#getRoomCount() <em>Get Room Count</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Room Count</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group25.residence.IResidenceProvider#getRoomCount()
	 * @generated
	 */
	EOperation getIResidenceProvider__GetRoomCount();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group25.residence.Room <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room</em>'.
	 * @see se.chalmers.cse.mdsd1617.group25.residence.Room
	 * @generated
	 */
	EClass getRoom();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group25.residence.Room#getRoomtype <em>Roomtype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Roomtype</em>'.
	 * @see se.chalmers.cse.mdsd1617.group25.residence.Room#getRoomtype()
	 * @see #getRoom()
	 * @generated
	 */
	EReference getRoom_Roomtype();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group25.residence.Room#getNum <em>Num</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Num</em>'.
	 * @see se.chalmers.cse.mdsd1617.group25.residence.Room#getNum()
	 * @see #getRoom()
	 * @generated
	 */
	EAttribute getRoom_Num();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ResidenceFactory getResidenceFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group25.residence.impl.ResidenceProviderImpl <em>Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group25.residence.impl.ResidenceProviderImpl
		 * @see se.chalmers.cse.mdsd1617.group25.residence.impl.ResidencePackageImpl#getResidenceProvider()
		 * @generated
		 */
		EClass RESIDENCE_PROVIDER = eINSTANCE.getResidenceProvider();

		/**
		 * The meta object literal for the '<em><b>Rooms</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESIDENCE_PROVIDER__ROOMS = eINSTANCE.getResidenceProvider_Rooms();

		/**
		 * The meta object literal for the '<em><b>Room Types</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESIDENCE_PROVIDER__ROOM_TYPES = eINSTANCE.getResidenceProvider_RoomTypes();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group25.residence.impl.RoomTypeImpl <em>Room Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group25.residence.impl.RoomTypeImpl
		 * @see se.chalmers.cse.mdsd1617.group25.residence.impl.ResidencePackageImpl#getRoomType()
		 * @generated
		 */
		EClass ROOM_TYPE = eINSTANCE.getRoomType();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group25.residence.IResidenceProvider <em>IResidence Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group25.residence.IResidenceProvider
		 * @see se.chalmers.cse.mdsd1617.group25.residence.impl.ResidencePackageImpl#getIResidenceProvider()
		 * @generated
		 */
		EClass IRESIDENCE_PROVIDER = eINSTANCE.getIResidenceProvider();

		/**
		 * The meta object literal for the '<em><b>Add Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRESIDENCE_PROVIDER___ADD_ROOM__INT_INT = eINSTANCE.getIResidenceProvider__AddRoom__int_int();

		/**
		 * The meta object literal for the '<em><b>Get Room Count</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRESIDENCE_PROVIDER___GET_ROOM_COUNT = eINSTANCE.getIResidenceProvider__GetRoomCount();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group25.residence.impl.RoomImpl <em>Room</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group25.residence.impl.RoomImpl
		 * @see se.chalmers.cse.mdsd1617.group25.residence.impl.ResidencePackageImpl#getRoom()
		 * @generated
		 */
		EClass ROOM = eINSTANCE.getRoom();

		/**
		 * The meta object literal for the '<em><b>Roomtype</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM__ROOMTYPE = eINSTANCE.getRoom_Roomtype();

		/**
		 * The meta object literal for the '<em><b>Num</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM__NUM = eINSTANCE.getRoom_Num();

	}

} //ResidencePackage
