/**
 */
package se.chalmers.cse.mdsd1617.group25;

import se.chalmers.cse.mdsd1617.group25.residence.IResidenceProvider;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hotel Customer Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group25.HotelCustomerProvides#getResidenceProvider <em>Residence Provider</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group25.Group25Package#getHotelCustomerProvides()
 * @model
 * @generated
 */
public interface HotelCustomerProvides extends IHotelCustomerProvides {

	/**
	 * Returns the value of the '<em><b>Residence Provider</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Residence Provider</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Residence Provider</em>' containment reference.
	 * @see #setResidenceProvider(IResidenceProvider)
	 * @see se.chalmers.cse.mdsd1617.group25.Group25Package#getHotelCustomerProvides_ResidenceProvider()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	IResidenceProvider getResidenceProvider();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group25.HotelCustomerProvides#getResidenceProvider <em>Residence Provider</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Residence Provider</em>' containment reference.
	 * @see #getResidenceProvider()
	 * @generated
	 */
	void setResidenceProvider(IResidenceProvider value);
} // HotelCustomerProvides
